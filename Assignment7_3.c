#include <stdio.h>
void verify();
void addition();
void multiplication();
void answer();
int main(){
  int x[10][10], y[10][10];
  int rowA,rowB,colA,colB;
  printf("Matrix Calculator (Addition and Multiplication)\n");
  printf("Matrix A Dimensions\n");
  printf("\tNo.of Rows: ");
  scanf("%d",&rowA);
  printf("\tNo.of Columns: ");
  scanf("%d",&colA);
  printf("\nMatrix B Dimensions\n");
  printf("\tNo.of Rows: ");
  scanf("%d",&rowB);
  printf("\tNo.of Columns: ");
  scanf("%d",&colB);
  verify(x, y, rowA, colA, rowB, colB);
  if(!((rowA==rowB)&&(colA==colB))) printf("\nCannot Perform Addition\n\tMathematical Error: A matrix can only be added to another matrix if the two matrices have the same dimensions\n");
  else addition(x,y,rowA,colB);
  if(!(colA==rowB)) printf("\nCannot Perform Multiplication\n\tMathematical Error: The number of columns in the first matrix must be equal to the number of rows in the second matrix to perform multiplication");
  else multiplication(x,y,rowA,colA,colB);
  return 0;
}
void verify(int x[10][10], int y[10][10], int rowA, int colA, int rowB, int colB){
  printf("\nInput Elements to the Matrix A:\n");
  for(int i=0; i<rowA; i++){
    for(int j=0; j<colA; j++){
      printf("Input Number [%d, %d]: ", i+1, j+1);
      scanf("%d",&x[i][j]);}}
  printf("\nInput Elements to the Matrix B:\n");
  for(int i=0; i<rowB; i++){
    for(int j=0; j<colB; j++){
      printf("Input Number [%d, %d]: ", i+1, j+1);
      scanf("%d", &y[i][j]);}}
}
void addition(int x[10][10],int y[10][10],int rowA,int colA){
  int add[10][10];
  for(int i=0;i<rowA;i++){
    for(int j=0;j<colA;j++){
       add[i][j]=x[i][j]+y[i][j];}}
  printf("\nAddition (Matrix A + Matrix B):\n");
  answer(add,rowA,colA);
}
void multiplication(int x[10][10],int y[10][10],int rowA,int colA,int colB){
  int mul[10][10];
  for(int i=0;i<rowA;i++){
    for(int j=0;j<colB;j++){
      mul[i][j]=0;
      for(int k=0;k<colA;k++){
        mul[i][j]=mul[i][j]+(x[i][k]*y[k][j]);}}}
   printf("\nMultiplication (Matrix A * Matrix B):\n");
   answer(mul,rowA,colB);
}
void answer(int x[10][10],int r,int c){
  for(int i=0;i<r;i++){
    for(int j=0;j<c;j++){
      printf("%d\t",x[i][j]);}
    printf("\n");}
}

